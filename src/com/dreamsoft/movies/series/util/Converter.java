package com.dreamsoft.movies.series.util;

import com.dreamsoft.movies.series.data.Movie;
import com.fasterxml.jackson.databind.JsonNode;

public class Converter {
	public static Movie json2Movie(JsonNode m){
		Movie movie = new Movie();
		if (m.get("name") != null)
		{
			movie.setTitle(m.get("name").asText());
		}
		if (m.get("poster") != null)
		{
			movie.setmPosterRes(m.get("poster").asText());
		}
		if (m.get("rating") != null)
		{
			movie.setFilmRating(m.get("rating").asText());
		}
		if (m.get("score") != null && !m.get("score").equals("null"))
		{
			movie.setScore(m.get("score").asDouble());
			if(movie.getScore() <= 0.0){
				movie.setScore(8.0);
			}
		}else{
			movie.setScore(8.0);
		}
		if (m.get("showtimes") != null)
		{
			movie.setShowTimes(m.get("showtimes").asText());
			if(movie.getShowTimes().trim().equalsIgnoreCase("null")){
				movie.setShowTimes("");
			}
		}else{
			movie.setShowTimes("");
		}
		if (m.get("title") != null)
		{
			movie.setTitle(m.get("title").asText());
		}
		if (m.get("id") != null)
		{
			movie.setId(m.get("id").asInt());
		}
		return movie;
	}
}
