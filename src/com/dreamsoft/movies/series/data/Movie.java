package com.dreamsoft.movies.series.data;

import java.io.Serializable;

public class Movie implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4292283541177822277L;
	/**
	 * 
	 */

	private String mTitle;
	private int id;
	private int mPosterResId;
	private String mPosterRes;
	private int chapId;

	// G, PG, PG-13, R
	private String mFilmRating;

	// Out of 5
	private Double mScore;

	private String mShowTimes;

	private int mDaysTillRelease;

	// Cached; calculated from showtimes
	//private List<Long> mShowTimeInUtcMillis;

	public String getTitle() {
		return mTitle;
	}
	public void setId(int id){
		this.id = id;
	}
	public int getId(){
		return this.id;
	}
	public void setTitle(String title) {
		mTitle = title;
	}

	public int getPosterResId() {
		return mPosterResId;
	}

	public void setPosterResId(int posterResId) {
		mPosterResId = posterResId;
	}

	public String getFilmRating() {
		return mFilmRating;
	}

	public void setFilmRating(String filmRating) {
		mFilmRating = filmRating;
	}

	public Double getScore() {
		return mScore;
	}

	public void setScore(Double score) {
		mScore = score;
	}

	public String getShowTimes() {
		return mShowTimes;
	}

	public void setShowTimes(String showTimes) {
		mShowTimes = showTimes;
/*
		mShowTimeInUtcMillis = new ArrayList<Long>();
		for (LocalTime time : showTimes) {
			DateTime utcDateTime = time.toDateTimeToday(DateTimeZone.UTC);
			mShowTimeInUtcMillis.add(utcDateTime.getMillis());
		}*/
	}

	/*public List<Long> getShowTimesInUtcMillis() {
		return mShowTimeInUtcMillis;
	}*/

	public int getDaysTillRelease() {
		return mDaysTillRelease;
	}

	public void setDaysTillRelease(int daysTillRelease) {
		mDaysTillRelease = daysTillRelease;
	}

	public String getmPosterRes() {
		return mPosterRes;
	}

	public void setmPosterRes(String mPosterRes) {
		this.mPosterRes = mPosterRes;
	}
	public int getChapId() {
		return chapId;
	}
	public void setChapId(int chapId) {
		this.chapId = chapId;
	}
}
