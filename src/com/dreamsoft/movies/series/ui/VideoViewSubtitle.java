
package com.dreamsoft.movies.series.ui;

import io.vov.vitamio.LibsChecker;
import io.vov.vitamio.MediaPlayer;
import io.vov.vitamio.MediaPlayer.OnTimedTextListener;
import io.vov.vitamio.widget.MediaController;
import io.vov.vitamio.widget.VideoView;

import org.apache.commons.lang.StringUtils;

import com.dreamsoft.movies.series.R;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;


public class VideoViewSubtitle extends Activity {

	private String path = "";
	private String subtitle_path = "";
	private VideoView mVideoView;
	private TextView mSubtitleView;
	private long mPosition = 0;
	private int mVideoLayout = 0;
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlistsub);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(65832139);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	@Override
	public void onCreate(Bundle icicle) {
		super.onCreate(icicle);
		if (!LibsChecker.checkVitamioLibs(this))
			return;
		requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.video_subtitle);
		loadAdview();
		mVideoView = (VideoView) findViewById(R.id.surface_view);
		MediaController ctr = new MediaController(this);		
		mVideoView.setMediaController(ctr);
		mSubtitleView = (TextView) findViewById(R.id.subtitle_view);
		mSubtitleView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 19);
		mSubtitleView.setTextColor(Color.WHITE);
		mSubtitleView.setTypeface(mSubtitleView.getTypeface(), Typeface.BOLD);
		Intent intent = getIntent();
		path = intent.getStringExtra("link");
		subtitle_path = intent.getStringExtra("sub");
		
		if (StringUtils.isEmpty(path)) {
			// Tell the user to provide a media file URL/path.
			Toast.makeText(VideoViewSubtitle.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			return;
		} else {
			/*
			 * Alternatively,for streaming media you can use
			 * mVideoView.setVideoURI(Uri.parse(URLstring));
			 */
			mVideoView.setVideoPath(path);
			//mVideoView.setVideoURI(Uri.parse(path));
			
			// mVideoView.setMediaController(new MediaController(this));
			mVideoView.requestFocus();

			mVideoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
				@Override
				public void onPrepared(MediaPlayer mediaPlayer) {
					// optional need Vitamio 4.0
					mediaPlayer.setPlaybackSpeed(1.0f);
					try{			
						mVideoView.addTimedTextSource(subtitle_path);
						mVideoView.setTimedTextShown(true);
					}catch(NullPointerException ex){
						//not exp
					}

				}
			});
			mVideoView.setOnTimedTextListener(new OnTimedTextListener() {

				@Override
				public void onTimedText(String text) {
					try{
						mSubtitleView.setText(text);
					}catch(NullPointerException ex){
						//not exp
					}
				}

				@Override
				public void onTimedTextUpdate(byte[] pixels, int width, int height) {

				}
			});
		}
	}

	@Override
	protected void onPause() {
		mPosition = mVideoView.getCurrentPosition();
		mVideoView.stopPlayback();
		super.onPause();
	}

	@Override
	protected void onResume() {
		if (mPosition > 0) {
			mVideoView.seekTo(mPosition);
			mPosition = 0;
		}
		super.onResume();
		mVideoView.start();
	}

	public void changeLayout(View view) {
		mVideoLayout++;
		if (mVideoLayout == 4) {
			mVideoLayout = 0;
		}
		switch (mVideoLayout) {
		case 0:
			mVideoLayout = VideoView.VIDEO_LAYOUT_ORIGIN;
			view.setBackgroundResource(R.drawable.mediacontroller_sreen_size_100);
			break;
		case 1:
			mVideoLayout = VideoView.VIDEO_LAYOUT_SCALE;
			view.setBackgroundResource(R.drawable.mediacontroller_screen_fit);
			break;
		case 2:
			mVideoLayout = VideoView.VIDEO_LAYOUT_STRETCH;
			view.setBackgroundResource(R.drawable.mediacontroller_screen_size);
			break;
		case 3:
			mVideoLayout = VideoView.VIDEO_LAYOUT_ZOOM;
			view.setBackgroundResource(R.drawable.mediacontroller_sreen_size_crop);

			break;
		}
		mVideoView.setVideoLayout(mVideoLayout, 0);
	}

}
