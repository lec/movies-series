package com.dreamsoft.movies.series.ui;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SectionIndexer;
import android.widget.TextView;
import android.widget.Toast;

import com.dreamsoft.movies.series.R;
import com.dreamsoft.movies.series.data.Movie;
import com.dreamsoft.movies.series.util.HttpUtil;
import com.dreamsoft.movies.series.util.StringMatcher;
import com.fasterxml.jackson.databind.JsonNode;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;


public class IndexableListViewActivity extends Activity {
	private ArrayList<String> mItems;
	private ArrayList<Movie> mMovies;
	private ListView mListView;
	private ProgressDialog progress;
	private String linkFilmBo = "http://phimhdplus.com/movies/api/chapter/";
	private String details = "http://phimhdplus.com/movies/api/details/";
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlistindex);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(65832139);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	private void showPleaseWait()
	{
		progress = ProgressDialog.show(this, getString(R.string.loadingTilte),
        	    getString(R.string.loadingWelcome), true);
	}
	private void hidePleaseWait()
	{
		if (progress != null)
    	{
    		progress.dismiss();
    		progress = null;
    	}
	}
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.indexable_list_movies);
        showPleaseWait();
        loadAdview();
		Intent intent = getIntent();
		int id = intent.getIntExtra("id", -1);
		if(id > 0)
		{
			GetMovie gM = new GetMovie();
			gM.execute(linkFilmBo + id);
		}else
		{
			Toast.makeText(this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			hidePleaseWait();
		}
    }
    
    private class ContentAdapter extends ArrayAdapter<String> implements SectionIndexer {
    	
    	private String mSections = "#ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    	
		public ContentAdapter(Context context, int textViewResourceId,
				List<String> objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public int getPositionForSection(int section) {
			// If there is no item for current section, previous section will be selected
			for (int i = section; i >= 0; i--) {
				for (int j = 0; j < getCount(); j++) {
					if (i == 0) {
						// For numeric section
						for (int k = 0; k <= 9; k++) {
							if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(k)))
								return j;
						}
					} else {
						if (StringMatcher.match(String.valueOf(getItem(j).charAt(0)), String.valueOf(mSections.charAt(i))))
							return j;
					}
				}
			}
			return 0;
		}

		@Override
		public int getSectionForPosition(int position) {
			return 0;
		}

		@Override
		public Object[] getSections() {
			String[] sections = new String[mSections.length()];
			for (int i = 0; i < mSections.length(); i++)
				sections[i] = String.valueOf(mSections.charAt(i));
			return sections;
		}
		
    }
    class GetMovie extends AsyncTask<String, Void, JsonNode>{
    	private boolean loadPlay = false;
		@Override
		protected JsonNode doInBackground(String... params) {
    		return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			if(result != null)
			{
				if (!loadPlay){
					loadFilmBo(result);
				}else{
					loadPlay(result);
				}

			}else
			{
				Toast.makeText(IndexableListViewActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
				hidePleaseWait();
			}
		}
	}
    private void loadPlay(JsonNode result)
    {
    	if (result != null){
			String servername = "g";
			if (result.get("servername") != null){
				servername = result.get("servername").asText();
			}
			Intent intent;
			if (servername.equalsIgnoreCase("y")){
				intent = new Intent(IndexableListViewActivity.this, YoutubeActivity.class);
			}
			else{
				intent = new Intent(IndexableListViewActivity.this, VideoViewSubtitle.class);
			}
			String sub = "";
			if (result.get("subtitle_vi") != null){
				sub = result.get("subtitle_vi").asText();
			}else if (result.get("subtitle_en") != null){
				sub = result.get("subtitle_en").asText();
			}
			String link = "";
			JsonNode linkNode = result.get("link");			
			if (servername.equalsIgnoreCase("y")){
				if (linkNode.get("linkPlay") != null){
					link = linkNode.get("linkPlay").asText();
				}
			}else if (linkNode.get("webm_480") != null && !linkNode.get("webm_480").asText().equals("null")){
				link = linkNode.get("webm_480").asText();
			}else if (linkNode.get("webm_360") != null && !linkNode.get("webm_360").asText().equals("null")){
				link = linkNode.get("webm_360").asText();
			}else if (linkNode.get("webm_720") != null && !linkNode.get("webm_720").asText().equals("null")){
				link = linkNode.get("webm_720").asText();
			}else if (linkNode.get("mp4_480") != null  && !linkNode.get("mp4_480").asText().equals("null")){
				link = linkNode.get("mp4_480").asText();
			}else if (linkNode.get("mp4_360") != null && !linkNode.get("mp4_360").asText().equals("null")){
				link = linkNode.get("mp4_360").asText();
			}else if (linkNode.get("mp4_720") != null && !linkNode.get("mp4_720").asText().equals("null")){
				link = linkNode.get("mp4_720").asText();
			}else if (linkNode.get("webm_1080") != null && !linkNode.get("webm_1080").asText().equals("null")){
				link = linkNode.get("webm_1080").asText();
			}else if (linkNode.get("mp4_1080") != null && !linkNode.get("mp4_1080").asText().equals("null")){
				link = linkNode.get("mp4_1080").asText();
			}
			else{
				Iterator<Entry<String, JsonNode>> it = linkNode.fields();
				while(it.hasNext()){
					Entry<String, JsonNode> entry = it.next();
					link = entry.getValue().asText();
					break;
				}
			}
			intent.putExtra("link", link);
			//sub = rootPath + "/PhimHDPlus/sub.srt";
			intent.putExtra("sub", sub);
			startActivity(intent);
		}
		hidePleaseWait();
    }
    private void loadFilmBo(JsonNode node)
    {
    	mItems = new ArrayList<String>();
    	mMovies = new ArrayList<Movie>();
    	if (node.get("chap") != null){
    		JsonNode rs = node.get("chap");
    		Iterator<JsonNode> it = rs.iterator();
    		while(it.hasNext())
    		{
    			JsonNode n = it.next();
    			Movie mo = new Movie();
    			if (n.get("name") != null){
    				mo.setTitle(n.get("name").asText());
    				mItems.add(n.get("name").asText());
    			}
    			if (n.get("idmovies") != null){
    				mo.setId(n.get("idmovies").asInt());
    			}
    			if (n.get("id") != null){
    				mo.setChapId(n.get("id").asInt());
    			}
    			mMovies.add(mo);
    		}
    	}
        Collections.sort(mItems);
        ArrayAdapter<String> adt = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, mItems.toArray(new  String[mItems.size()]));
       
        mListView = (ListView) findViewById(R.id.listview);
        mListView.setAdapter(adt);
        mListView.setFastScrollEnabled(true);
        mListView.setOnItemClickListener(new OnItemClickListener(){
			@Override
			public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				showPleaseWait();
				TextView tv = (TextView) arg1;
				String chap = tv.getText().toString().trim().toUpperCase();
				Movie curr = null;
				for (Movie m : mMovies){
					if (chap.equalsIgnoreCase(m.getTitle().trim().toUpperCase())){
						curr = m;
						break;
					}
				}
				if (curr != null){
					GetMovie gM = new GetMovie();
					gM.loadPlay = true;
					gM.execute(details + curr.getId() + "/" + curr.getChapId());
				}
			}
		});
		hidePleaseWait();
    }
}