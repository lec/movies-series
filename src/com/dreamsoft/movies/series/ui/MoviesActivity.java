package com.dreamsoft.movies.series.ui;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.dreamsoft.movies.series.R;
import com.dreamsoft.movies.series.data.Movie;
import com.dreamsoft.movies.series.util.Converter;
import com.dreamsoft.movies.series.util.HttpUtil;
import com.dreamsoft.movies.series.util.InternetUtil;
import com.dreamsoft.movies.series.util.MockUtils;
import com.dreamsoft.movies.series.util.Vietnamese;
import com.dreamsoft.movies.series.view.SlidingListView;
import com.dreamsoft.movies.series.view.ViewPager;
import com.dreamsoft.movies.series.widget.MovieAdapter;
import com.dreamsoft.movies.series.widget.MovieAdapter.MovieAdapterListener;
import com.fasterxml.jackson.databind.JsonNode;
import com.smaato.soma.AdType;
import com.smaato.soma.BannerView;

public class MoviesActivity extends FragmentActivity implements ActionBar.TabListener,
		MovieAdapter.MovieAdapterListener {

	private ProgressDialog progress;
	private InternetUtil internet;
	SectionsPagerAdapter mSectionsPagerAdapter;
	private String filmBo = "http://phimhdplus.com/movies/api/2";
	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager mViewPager;
	public static String[] COUNTRIES = new String[] {};

	private SlidingListView mListView;
	private MovieAdapter mAdapter;
	private String rootPath;
	public static LayoutInflater inflator;
	public static List<Movie> search = new ArrayList<Movie>();
	private void loadAdview() {
		BannerView mBanner = (BannerView) findViewById(R.id.bannerViewlist);
		mBanner.getAdSettings().setAdType(AdType.ALL);
		mBanner.getAdSettings().setAdspaceId(65832139);
		mBanner.getAdSettings().setPublisherId(923876207);
		mBanner.asyncLoadNewBanner();
	}
	public void adapAutocomplete(){
        getActionBar().setDisplayShowCustomEnabled(true);
		List<String> l = new ArrayList<String>();
		for (Movie m : search){
			l.add(m.getTitle());
			l.add(Vietnamese.toUrlFriendly(m.getTitle()));
		}
		COUNTRIES = l.toArray(new String[l.size()]);
		View v = inflator.inflate(R.layout.autocomplete, null);
        getActionBar().setCustomView(v);
        
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(MoviesActivity.this,
                android.R.layout.simple_dropdown_item_1line, COUNTRIES);
        
        AutoCompleteTextView textView = (AutoCompleteTextView) v
                .findViewById(R.id.editText1);
        textView.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(textView, InputMethodManager.SHOW_IMPLICIT);
        textView.setOnItemClickListener(searchAutocomplete);
        textView.setAdapter(adapter);
	}
	public AdapterView.OnItemClickListener searchAutocomplete =  new OnItemClickListener() {

		@Override
		public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
				long arg3) {
			TextView tv = (TextView)arg1;
			String search = tv.getText().toString().trim().toLowerCase();
			int id = 0;
			for(Movie m : MoviesActivity.search){
				String title = m.getTitle();
				if (title.trim().toLowerCase().equals(search) 
						|| Vietnamese.toUrlFriendly(title).trim().toLowerCase().equals(search)){
					id = m.getId();
					break;
				}
			}
			if (id > 0){
				if (internet.haveNetworkConnection()){
					Intent itent = new Intent(MoviesActivity.this, IndexableListViewActivity.class);
					itent.putExtra("id", id);
					startActivity(itent);
				}
				else
				{
					Toast.makeText(MoviesActivity.this, getString(R.string.noInternet), Toast.LENGTH_LONG).show();
				}
			}
		}
    	
	};
	private String getRootPathStorage()
	{
		String path = null;
		Boolean isSDPresent = Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED);
	    if(isSDPresent)
	    {
	      path = Environment.getExternalStorageDirectory().getAbsolutePath();
	    }
	    else
	    {
	        path = getFilesDir().getAbsolutePath();
	    }
	    return path;
	}
	private void loadMovies() {
		File dir = new File(rootPath + getString(R.string.movieFolder));
		File data = new File(rootPath + getString(R.string.movieData));
		if (!dir.exists() || !data.exists())
		{
			//first time load 
			if (internet.haveNetworkConnection())
			{
				dir.mkdir();
				GetMovies gMovie = new GetMovies();
				gMovie.setLoadImediatly(true);
				gMovie.execute(filmBo);
			}
			else
			{
				Toast.makeText(this, getString(R.string.noInternet), Toast.LENGTH_SHORT).show();
				hidePleaseWait();
				return;
			}
		}
		else
		{
			GetMoviesOffline getOffline = new GetMoviesOffline();
			getOffline.execute("getoffline");
		}
	}
    private void showPleaseWait()
	{
		progress = ProgressDialog.show(this, getString(R.string.loadingTilte),
        	    getString(R.string.loadingWelcome), true);
	}
	private void hidePleaseWait()
	{
		if (progress != null)
    	{
    		progress.dismiss();
    		progress = null;
    	}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_movies);
		showPleaseWait();
		loadAdview();

		internet = new InternetUtil();
		internet.setActivity(this);
		rootPath = getRootPathStorage();
		// Set up the action bar.
		final ActionBar actionBar = getActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

		// Create the adapter that will return a fragment for each of the three
		// primary sections of the app.
		mSectionsPagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		mViewPager = (ViewPager) findViewById(R.id.pager);
		mViewPager.setAdapter(mSectionsPagerAdapter);
		mViewPager.setOffscreenPageLimit(3);
		inflator = (LayoutInflater) this
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		// For each of the sections in the app, add a tab to the action bar.
		for (int i = 0; i < mSectionsPagerAdapter.getCount(); i++) {
			// Create a tab with text corresponding to the page title defined by
			// the adapter. Also specify this Activity object, which implements
			// the TabListener interface, as the callback (listener) for when
			// this tab is selected.
			actionBar.addTab(
					actionBar.newTab()
							.setText(mSectionsPagerAdapter.getPageTitle(i))
							.setTabListener(this));
		}
		mViewPager.setCurrentItem(1);
		actionBar.setSelectedNavigationItem(1);

		mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
			private int mLastState;

			@Override
			public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
				if (position == 0) {
					setSlide(positionOffset - 1);
				}
				else if (position == 1) {
					setSlide(positionOffset);
				}
				else if (position == 2) {
					setSlide(1);
				}
			}

			@Override
			public void onPageSelected(int position) {
				actionBar.setSelectedNavigationItem(position);
			}

			@Override
			public void onPageScrollStateChanged(int state) {
				if (state == ViewPager.SCROLL_STATE_IDLE) {
					mListView.setUseHardwareLayers(false);
				}
				else if (mLastState == ViewPager.SCROLL_STATE_IDLE) {
					mListView.setUseHardwareLayers(true);
				}

				mLastState = state;
			}
		});

		mListView = (SlidingListView) findViewById(R.id.sliding_list_view);

		// Add some spacing views above/below the rest of the rows; this keeps us from having
		// to customize the first/last rows to add some extra padding
		LayoutInflater inflater = LayoutInflater.from(this);
		mListView.addHeaderView(inflater.inflate(R.layout.include_header_footer_space, mListView, false));
		mListView.addFooterView(inflater.inflate(R.layout.include_header_footer_space, mListView, false));

		// We want the total width == 3*cell padding + 2*cell size
		int cellPadding = getResources().getDimensionPixelSize(R.dimen.cell_padding);
		Point size = new Point();
		getWindowManager().getDefaultDisplay().getSize(size);
		cellSize = (size.x - 3 * cellPadding) / 2;
		movieAdapterListener = this;
		mAdapter = new MovieAdapter(MoviesActivity.this, new ArrayList<Movie>(), this, cellSize);
		loadMovies();
	}
	private MovieAdapterListener movieAdapterListener;
	private int cellSize;
	private void setSlide(float slide) {
		mAdapter.setSlide(slide);
		mListView.setSlide(slide);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.movies, menu);
		return true;
	}
	int check = 0;
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.action_rate:
				Intent intent = new Intent(Intent.ACTION_VIEW);
				intent.setData(Uri.parse("market://details?id=" + getApplication().getPackageName()));
				startActivity(intent);
				return true;
			case R.id.action_about:
				DialogFragment df = new AboutDialogFragment();
				df.show(getSupportFragmentManager(), "aboutDf");
				
				return true;
			case R.id.action_web:
				Intent intentweb = new Intent(Intent.ACTION_VIEW);
				intentweb.setData(Uri.parse("http://phimhdplus.com"));
				startActivity(intentweb);
				return true;
			case R.id.action_search:
				if (check == 0)
				{
					adapAutocomplete();
					check = 1;
				}
				else{
					
					check = 0;
					getActionBar().setDisplayShowCustomEnabled(false);
				}
				return true;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
		// When the given tab is selected, switch to the corresponding page in
		// the ViewPager.
		mViewPager.setCurrentItem(tab.getPosition());
	}
	private static long back_pressed;
	@Override
	public void onBackPressed() {
		// If the user is looking at detailed rows, put them back to the other screen instead
		// of leaving the page entirely
		if (mViewPager.getCurrentItem() != 1) {
			mViewPager.setCurrentItem(1, true);
		}
		else {
			if (back_pressed + 2000 > System.currentTimeMillis())
			{
				super.onBackPressed();
			}
			else
			{
				Toast.makeText(this, getString(R.string.doubleClickMsg), Toast.LENGTH_SHORT).show();
				back_pressed = System.currentTimeMillis();
			}
		}
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
	}

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {

		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return new SpaceFragment();
		}

		@Override
		public int getCount() {
			// Show 3 total pages.
			return 3;
		}

		@Override
		public long getItemId(int position) {
			return super.getItemId(position);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(R.string.title_section1).toUpperCase(l);
			case 1:
				return getString(R.string.title_section2).toUpperCase(l);
			case 2:
				return getString(R.string.title_section3).toUpperCase(l);
			}
			return null;
		}
	}

	/**
	 * A simple Fragment that just takes up space; We just want to use
	 * the Decor View on top for display.
	 */
	public static class SpaceFragment extends Fragment {
		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container,
				Bundle savedInstanceState) {
			return inflater.inflate(R.layout.fragment_space, container, false);
		}
	}

	//////////////////////////////////////////////////////////////////////////
	// MovieAdapterListener

	@Override
	public void onMovieClicked(Movie movie, boolean isOnLeft) {
		int targetItem = isOnLeft ? 0 : 2;
		if (mViewPager.getCurrentItem() != targetItem) {
			mViewPager.setCurrentItem(targetItem, true);
		}
		else
		{
			if (internet.haveNetworkConnection()){
				Intent itent = new Intent(MoviesActivity.this, IndexableListViewActivity.class);
				itent.putExtra("id", movie.getId());
				startActivity(itent);
			}
			else
			{
				Toast.makeText(MoviesActivity.this, getString(R.string.noInternet), Toast.LENGTH_LONG).show();
			}
		}
	}
	class GetMovies extends AsyncTask<String, Void, JsonNode>{
		private boolean loadImediatly = false;
		public void setLoadImediatly(boolean isSet){
			loadImediatly = isSet;
		}
		@Override
		protected JsonNode doInBackground(String... params) {
    		return HttpUtil.getJson(params[0]);
		}
		
		@Override
		protected void onPostExecute(JsonNode result) {
			List<Movie> movies = new ArrayList<Movie>();
			if (result != null){
				Iterator<JsonNode> ite = result.iterator();
				while(ite.hasNext()){
					movies.add(Converter.json2Movie(ite.next()));
				}
			}
			try {
				if (movies.size() > 0){
					MockUtils.writeDataToFileSystem(rootPath + getString(R.string.movieData), movies);					
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if (loadImediatly){
				mAdapter = new MovieAdapter(MoviesActivity.this, movies, movieAdapterListener, cellSize);
				MoviesActivity.search = movies;
				mListView.setAdapter(mAdapter);
				hidePleaseWait();
			}
		}
	}
	class GetMoviesOffline extends AsyncTask<String, Void, List<Movie>>{
		@Override
		protected List<Movie> doInBackground(String... params) {
			List<Movie> m = new ArrayList<Movie>();
			try {
				
				m = (ArrayList<Movie>)MockUtils.readDataFromFileSystem(rootPath + getString(R.string.movieData));
				Object ob = MockUtils.readDataFromFileSystem(rootPath + getString(R.string.movieData));
				m = (ArrayList)ob;
			} catch (FileNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}catch(Exception e)
			{
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			}
			catch(Error e)
			{
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_LONG).show();
			}
    		return m;
		}
		
		@Override
		protected void onPostExecute(List<Movie> result) {
			if (!result.isEmpty())
			{
				mAdapter = new MovieAdapter(MoviesActivity.this, result, movieAdapterListener, cellSize);
				MoviesActivity.search = result;
				mListView.setAdapter(mAdapter);
				GetMovies gMovie = new GetMovies();
				gMovie.execute(filmBo);
				hidePleaseWait();
				return;
			}else if (internet.haveNetworkConnection()){
				GetMovies gMovie = new GetMovies();
				gMovie.setLoadImediatly(true);
				gMovie.execute(filmBo);
			}else{
				Toast.makeText(MoviesActivity.this, getString(R.string.serverBusy), Toast.LENGTH_SHORT).show();
				hidePleaseWait();
			}			
		}
	}
	
}
